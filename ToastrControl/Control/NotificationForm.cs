﻿using System;
using System.Drawing;
using System.Windows.Forms;
using ToastrControl.Properties;

namespace ToastrControl.Control
{
    public partial class NotificationForm : Form
    {
        public NotificationForm()
        {
            InitializeComponent();
            timer = new Timer();
            timer.Tick += Timer_Tick;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            switch (action)
            {
                case ActionEnum.Start:
                    timer.Interval = 1;
                    this.Opacity += 0.1;
                    if (this.x < this.Location.X)
                    {
                        this.Left--;
                    }
                    else
                    {
                        if (this.Opacity == 1.0)
                        {
                            action = ActionEnum.Wait;
                        }
                    }
                    break;
                case ActionEnum.Wait:
                    timer.Interval = 5000;
                    action = ActionEnum.Close;
                    break;
                case ActionEnum.Close:
                    this.timer.Interval = 1;
                    this.Opacity -= 0.1;
                    this.Left -= 3;

                    if (base.Opacity == 0.0)
                        base.Close();
                    break;
                default:
                    break;
            }
        }

        private Timer timer;
        private ActionEnum action;
        private int x;
        private int y;

        public void ShowAlert(string message,NotificationTypeEnum notificationType)
        {
            this.Opacity = 0;
            this.StartPosition = FormStartPosition.Manual;
            string fname;

            for (int i = 1; i < 10; i++)
            {
                fname = "alert" + i.ToString();

                NotificationForm form = (NotificationForm)Application.OpenForms[fname];

                if (form == null)
                {
                    this.Name = fname;
                    this.x = Screen.PrimaryScreen.WorkingArea.Width - this.Width + 15;
                    this.y = Screen.PrimaryScreen.WorkingArea.Height - this.Height * i;
                    this.Location = new Point(this.x, this.y);
                    break;
                }
            }

            this.x = Screen.PrimaryScreen.WorkingArea.Width - base.Width - 5;

            switch (notificationType)
            {
                case NotificationTypeEnum.Success:
                    pictureBox1.Image = Resources.Success;
                    this.BackColor = Color.FromArgb(116, 181, 116);
                    break;
                case NotificationTypeEnum.Info:
                    pictureBox1.Image = Resources.Info;
                    this.BackColor = Color.FromArgb(89, 171, 195);
                    break;
                case NotificationTypeEnum.Warning:
                    pictureBox1.Image = Resources.Warning;
                    this.BackColor = Color.FromArgb(248, 148, 6);
                    break;
                case NotificationTypeEnum.Error:
                    pictureBox1.Image = Resources.Error;
                    this.BackColor = Color.FromArgb(189, 54, 47);
                    break;
                default:
                    break;
            }

            this.lblMessage.Text = message;
            this.Show();
            this.action = ActionEnum.Start;
            this.timer.Interval = 1;
            this.timer.Start();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            timer.Interval = 1;
            action = ActionEnum.Close;
        }
    }

    public enum ActionEnum
    {
        Start,
        Wait,
        Close
    }

    public enum NotificationTypeEnum
    {
        Success,
        Info,
        Warning,
        Error
    }
}
