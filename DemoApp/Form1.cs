﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ToastrControl.Control;

namespace DemoApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ShowNotif(NotificationTypeEnum type)
        {
            var form = new NotificationForm();
            form.ShowAlert(txtMessage.Text.Trim(), type);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (rdBtnSuccess.Checked)
                ShowNotif(NotificationTypeEnum.Success);
            else
                if (rdBtnInfo.Checked)
                ShowNotif(NotificationTypeEnum.Info);
            else if (rdBtnWarning.Checked)
                ShowNotif(NotificationTypeEnum.Warning);
            else if (rdBtnError.Checked)
                ShowNotif(NotificationTypeEnum.Error);


        }
    }
}
