﻿namespace DemoApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdBtnSuccess = new System.Windows.Forms.RadioButton();
            this.rdBtnInfo = new System.Windows.Forms.RadioButton();
            this.rdBtnWarning = new System.Windows.Forms.RadioButton();
            this.rdBtnError = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtMessage
            // 
            this.txtMessage.Location = new System.Drawing.Point(107, 41);
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(421, 20);
            this.txtMessage.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(547, 39);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Show";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdBtnError);
            this.groupBox1.Controls.Add(this.rdBtnWarning);
            this.groupBox1.Controls.Add(this.rdBtnInfo);
            this.groupBox1.Controls.Add(this.rdBtnSuccess);
            this.groupBox1.Location = new System.Drawing.Point(107, 78);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 167);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // rdBtnSuccess
            // 
            this.rdBtnSuccess.AutoSize = true;
            this.rdBtnSuccess.Checked = true;
            this.rdBtnSuccess.Location = new System.Drawing.Point(28, 34);
            this.rdBtnSuccess.Name = "rdBtnSuccess";
            this.rdBtnSuccess.Size = new System.Drawing.Size(66, 17);
            this.rdBtnSuccess.TabIndex = 0;
            this.rdBtnSuccess.TabStop = true;
            this.rdBtnSuccess.Text = "Success";
            this.rdBtnSuccess.UseVisualStyleBackColor = true;
            // 
            // rdBtnInfo
            // 
            this.rdBtnInfo.AutoSize = true;
            this.rdBtnInfo.Location = new System.Drawing.Point(28, 68);
            this.rdBtnInfo.Name = "rdBtnInfo";
            this.rdBtnInfo.Size = new System.Drawing.Size(43, 17);
            this.rdBtnInfo.TabIndex = 1;
            this.rdBtnInfo.Text = "Info";
            this.rdBtnInfo.UseVisualStyleBackColor = true;
            // 
            // rdBtnWarning
            // 
            this.rdBtnWarning.AutoSize = true;
            this.rdBtnWarning.Location = new System.Drawing.Point(28, 101);
            this.rdBtnWarning.Name = "rdBtnWarning";
            this.rdBtnWarning.Size = new System.Drawing.Size(65, 17);
            this.rdBtnWarning.TabIndex = 2;
            this.rdBtnWarning.Text = "Warning";
            this.rdBtnWarning.UseVisualStyleBackColor = true;
            // 
            // rdBtnError
            // 
            this.rdBtnError.AutoSize = true;
            this.rdBtnError.Location = new System.Drawing.Point(28, 133);
            this.rdBtnError.Name = "rdBtnError";
            this.rdBtnError.Size = new System.Drawing.Size(47, 17);
            this.rdBtnError.TabIndex = 3;
            this.rdBtnError.Text = "Error";
            this.rdBtnError.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Message";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 301);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtMessage);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdBtnError;
        private System.Windows.Forms.RadioButton rdBtnWarning;
        private System.Windows.Forms.RadioButton rdBtnInfo;
        private System.Windows.Forms.RadioButton rdBtnSuccess;
        private System.Windows.Forms.Label label1;
    }
}

